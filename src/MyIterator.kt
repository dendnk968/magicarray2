 import kotlin.collections.Iterator

class MyIterator<T>(private val array: MagicArray<T>): Iterator<T> {
    private var id = 0
    /**
     * Returns `true` if the iteration has more elements.
     */
    override fun hasNext(): Boolean {
        return id < array.size
    }

    /**
     * Returns the next element in the iteration.
     */
    override fun next(): T {
        return array.get(id++)
    }
}