class Node<T>(val y: Int, var date: T, val leftChild: Node<T>? = null, val rightChild: Node<T>? = null) {
    val size: Int = (leftChild?.size ?: 0) + (rightChild?.size ?: 0) + 1

    fun merge(right: Node<T>?): Node<T> {
        right ?: return this
        if (y > right.y) {
            rightChild ?: return Node(y, date, leftChild, right)
            return Node(y, date, leftChild, rightChild.merge(right))
        }
        return Node(right.y, right.date, this.merge(right.leftChild), right.rightChild)
    }

    fun split(ind: Int): Pair<Node<T>?, Node<T>?> {
        if (ind < 0)
            return Pair(null, this)
        if (ind > size)
            return Pair(this, null)
        val ind_self = leftChild?.size ?: 0
        if (ind_self >= ind) {
            val left = leftChild?.split(ind) ?: Pair(null, null)
            return Pair(left.first, Node(y, date, left.second, rightChild))
        }
        val right = rightChild?.split(ind - ind_self - 1) ?: Pair(null, null)
        return Pair(Node(y, date, leftChild, right.first),right.second)
    }


    fun contains(x: T): Boolean = x == date || (leftChild?.contains(x) ?: false) || (rightChild?.contains(x) ?: false)
}








