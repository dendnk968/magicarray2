import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.PrintWriter
import java.util.*
//test
//comment
/**
 *
 * @param T
 * @property root Node<T>?
 * @property random Random
 * @property size Int
 */
class MagicArray<T> : Iterable<T>{
    /**
     * Returns an iterator over the elements of this object.
     */
    override fun iterator(): Iterator<T> {
        return MyIterator<T>(this)
    }

    private var root: Node<T>? = null
    private val random = Random()

    constructor() {}

    constructor(root: Node<T>?) {
        this.root = root
    }

    var size = 0
        private set

    /**
     *
     * @param element T
     * @return Boolean
     */
    fun contains(element: T): Boolean = root?.contains(element) ?: false

    /**
     *
     * @param elements Collection<T>
     * @return Boolean
     */
    fun containsAll(elements: Collection<T>): Boolean = elements.all { t -> root?.contains(t) ?: false }

    /**
     *
     * @param index Int
     * @return T
     */
    operator fun get(index: Int): T {
        checkIndex(index)
        val spl = root!!.split(index + 1)
        val spl2 = spl.first!!.split(index)
        return spl2.second!!.date
    }

    /**
     *
     * @param el T
     */
    fun add(el: T) {
        size++
        val node = Node(random.nextInt(), el)
        root = root?.merge(node) ?: node
    }

    /**
     *
     * @param array MagicArray<T>
     */
    fun add(array: MagicArray<T>) {
        size += array.size
        root = root?.merge(array.root) ?: array.root
    }

    /**
     *
     * @param array Collection<T>
     */
    fun add(array: Collection<T>) {
        array.forEach { this.add(it) }
    }

    private fun checkIndex(index: Int) {
        root ?: throw ArrayIndexOutOfBoundsException()
        if (index < 0 || index > size)
            throw ArrayIndexOutOfBoundsException()
    }

    /**
     *
     * @return Boolean
     */
    fun isEmpty(): Boolean = size == 0

    /**
     *
     * @param fromIndex Int
     * @param toIndex Int
     * @return MagicArray<T>
     */
    fun subList(fromIndex: Int, toIndex: Int): MagicArray<T> {
        checkIndex(fromIndex)
        checkIndex(toIndex)
        val t = MagicArray<T>(root?.split(toIndex)?.first?.split(fromIndex)?.second)
        t.size = toIndex - fromIndex
        return t
    }

    /**
     *
     * @param fileName String
     */
    fun saveToFile(fileName: String) {
        val outFile = PrintWriter(FileOutputStream(fileName))
        for (i in 0 until size)
            outFile.println(this[i])
    }

    fun delete(from: Int, to: Int): MagicArray<T> {


        checkIndex(from)
        checkIndex(to)
        val split = root?.split(from)
        val split2 = split?.second?.split(to)

        val t = MagicArray<T>(split?.first?.merge(split2?.second) ?: split2?.second)
        t.size = size - to + from
        return t
    }

    companion object {
        fun getFromFileIntMagicArray(fileName: String): MagicArray<Int> {
            val input = Scanner(FileInputStream(fileName))
            val ans = MagicArray<Int>()
            while (input.hasNext()) {
                ans.add(input.nextInt())
            }
            return ans
        }
    }
}