import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import kotlin.test.assertFails

internal class MagicArrayTest {

    @Test
    fun test() {
        val arr = MagicArray<Int>()
        for (i in 0..5) {
            arr.add(i)
        }
        assertEquals(6, arr.size)
        for (i in 0..5) {
            assertEquals(i, arr.get(i))
        }
        
    }

    @Test
    fun indexBound() {
        val arr = MagicArray<Int>()
        try {
            arr.get(-1)
            fail<Unit>()
        } catch (ex: ArrayIndexOutOfBoundsException){

        }

    }

    @Test
    fun subListTest(){
        var arr = MagicArray<Int>()
        for (i in 0..5) {
            arr.add(i)
        }
        val ans = arr.subList(1, 3)
        assertEquals(2, ans.size)
        for (i in 0..1){
            assertEquals(i + 1, ans.get(i))
        }
    }
}